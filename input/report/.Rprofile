# set environment variables
Sys.setenv(RENV_PATHS_LOCKFILE = normalizePath("renv/renv.lock",
                                               winslash = "/",
                                               mustWork = TRUE))
# set R options
options(
  # base R options
  warnPartialMatchArgs = TRUE,
  warnPartialMatchAttr = TRUE,
  warnPartialMatchDollar = TRUE,
  # package options
  ggplot2.discrete.colour = \(...) viridis::scale_color_viridis(..., discrete = TRUE, option = "viridis", end = 0.925),
  ggplot2.discrete.fill = \(...) viridis::scale_fill_viridis(..., discrete = TRUE, option = "viridis", end = 0.925),
  gt.html_tag_check = TRUE,
  renv.config.pak.enabled = FALSE, # disabled since it isn't stable yet, cf. https://github.com/rstudio/renv/issues/1210
  renv.config.updates.parallel = 8L,
  rmarkdown.html_dependency.header_attr = FALSE,
  salim.font_family_body = "Alegreya Sans",
  salim.plot_color_body = "#343a40",
  salim.plot_color_bg = "#f2f2f2",
  salim.plot_color_grid = "#d9d9d9"
)

# load renv
source("renv/activate.R")

# pkg loading and rlang stuff
## attach magrittr pipe operators
if (nzchar(system.file(package = "magrittr"))) {
  library(magrittr,
          include.only = c("%>%", "%<>%", "%T>%", "%!>%", "%$%"))
}

if (nzchar(system.file(package = "rlang"))) {

  ## register rlang's default global handlers
  rlang::global_handle()

  ## attach rlang operators
  library(rlang,
          include.only = "%|%")
}

## load pkg gt to trigger its `.onLoad()` code (otherwise tables won't be properly rendered by knitr)
## TODO: create reprex and submit issue about this
##       weird thing: after initial rstudio startup, gt tables are rendered fine; but after an R session restart, not anymore
if (nzchar(system.file(package = "gt"))) {
  invisible(loadNamespace("gt"))
}

# set RNG seeds
set.seed(42L)

if (nzchar(system.file(package = "htmlwidgets"))) {
  htmlwidgets::setWidgetIdSeed(seed = 42L)
}

# load font for showtext
# TODO: specify emoji font via `symbol` arg once `sysfonts::font_add_google()` (in combo with `showtext::showtext_auto()`) supports this,
#       cf. https://github.com/yixuan/showtext/issues/54
if (nzchar(system.file(package = "sysfonts"))) {
  sysfonts::font_add_google(name = "Alegreya Sans")
}

if (nzchar(system.file(package = "showtext"))) {
  # set showtext's DPI to `ggplot2::ggsave()`'s default DPI value
  # cf. https://community.rstudio.com/t/font-gets-really-small-when-saving-to-png-using-ggsave-and-showtext/147029/7
  showtext::showtext_opts(dpi = 300L)

  # enable showtext for graphics devices
  showtext::showtext_auto()
}

# set common ggplot2 geom defaults
if (nzchar(system.file(package = "salim"))) {
  salim::ggplot2_geom_defaults(family = "Alegreya Sans")
}

# run code after RStudio is launched (i.a. to be able to see progress and output)
setHook(hookName = "rstudio.sessionInit",
        value = function(newSession) {

          # restore renv state from lockfile
          show_progress <- nzchar(system.file(package = "cli")) && interactive()

          if (show_progress) {
            cli_id <- cli::cli_progress_step(msg = "Restoring renv state...",
                                             msg_done = "Restoring renv state done. Ready to go.",
                                             .auto_close = FALSE)
          }

          renv::restore(clean = TRUE,
                        prompt = FALSE)

          if (show_progress) {
            cli::cli_progress_done(id = cli_id)
          }

          # level up R, RStudio and Quarto
          if (nzchar(system.file(package = "salim"))) {
            salim::lvl_up_r(path_min_vrsn = "../.R_version",
                            update_min_vrsn = isTRUE(Sys.info()[["user"]] == "salim"))
            salim::lvl_up_quarto(path_min_vrsn = "../.quarto_version",
                                 update_min_vrsn = isTRUE(Sys.info()[["user"]] == "salim"))
            salim::lvl_up_rstudio(path_min_vrsn = "../.RStudio_version",
                                  update_min_vrsn = isTRUE(Sys.info()[["user"]] == "salim"))
          }
        },
        action = "append")
