```{r}
#| include: false

data_temp <-
  rdb.report::dataset(part = "regional") %>%
  dplyr::filter(dplyr::between(year, 1900, 2019) & un_region_tier_1_name == "Americas") %>%
  rdb::as_ballot_dates() %>%
  dplyr::arrange(date)
```

Looking only at ballot dates in the *Americas* (@fig-ballot-dates-per-decade-by-subregion-americas), we find the following:

-   From 1900 to 2019, `r nrow(data_temp)` ballot dates have taken place on the American continent.

-   Countries in *South America*[^_intro-1] hold between 25 % (1960s/1970s) and 100 % (1900s) of all ballot dates in the Americas.

-   Countries in the *Caribbean*[^_intro-2] hold between 20 % (1940s) and 70 % (1970s) of all ballot dates in the Americas.

-   In *Central America*[^_intro-3], the use of ballot dates has stayed quite constant between 5 % and 10 % since the 1930s.

-   Countries in *Northern America*[^_intro-4] hold between 5 % (2010s) and 30 % (1940s) of all ballot dates in the Americas.

```{r}
#| label: fig-ballot-dates-per-decade-by-subregion-americas
#| fig-cap: "Share of American ballot dates per decade 1900–2019, by subregion"
#| fig-column: page

rdb.report::insert_plotly(id = "ballot_dates_per_decade_by_subregion_americas",
                          htmlwidget = TRUE,
                          htmlwidget_asis = FALSE)
```

[^_intro-1]: `r data_temp %>% dplyr::filter(un_subregion == "South America") %$% country_name %>% unique() %>% sort() %>% pal::enum_str()`

[^_intro-2]: `r data_temp %>% dplyr::filter(un_subregion == "Caribbean") %$% country_name %>% unique() %>% sort() %>% pal::enum_str()`

[^_intro-3]: `r data_temp %>% dplyr::filter(un_subregion == "Central America") %$% country_name %>% unique() %>% sort() %>% pal::enum_str()`

[^_intro-4]: `r data_temp %>% dplyr::filter(un_subregion == "Northern America") %$% country_name %>% unique() %>% sort() %>% pal::enum_str()`
