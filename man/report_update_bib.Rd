% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/rdb.report.gen.R
\name{report_update_bib}
\alias{report_update_bib}
\title{Update bibliography file}
\usage{
report_update_bib(
  path = path_pkg_repo("input/report/assets/bibliography.json"),
  force = FALSE,
  show_progress = TRUE
)
}
\arguments{
\item{path}{Path to write the bibliography file to. A character scalar.}

\item{force}{Whether or not to enforce overwriting the bibliography file regardless of actual changes to the Zotero library since the last export.}

\item{show_progress}{Whether or not to output progress indication.}
}
\value{
\code{path}, invisibly.
}
\description{
Writes all items in the public Zotero group library \href{https://www.zotero.org/groups/4604812/rdb_report}{\code{rdb_report}} to the CSL-JSON bibliography file
specified in \code{path} (by default \code{input/report/assets/bibliography.json}) if the corresponding bibliography version file
(\code{input/report/assets/bibliography.version} by default) does not exist or signifies an outdated version.
}
\seealso{
Other functions to manage report's asset files:
\code{\link{report_update_csl}()}
}
\concept{report_assets}
